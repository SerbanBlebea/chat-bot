<?php
namespace App;

class Router
{
    public $base_url;
    public $params = array();
    public $routes = array();
    public $foundRoute = false;

    public function __construct()
    {
        $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
        if (strstr($uri, '?'))
        {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $uri = '/' . trim($uri, '/');
        return $this->base_url = $uri;
    }

    public function getParams($uri)
    {
        if (strstr($uri, '?'))
        {
            $arrayUri = explode('?', $uri);
            $params = $arrayUri[1];
            $params = explode('&', $params);

            $paramsArray = array();

            foreach($params as $param)
            {
                $param = explode('=', $param);
                $paramsArray[$param[0]] =  $param[1];
            }
            return $paramsArray;
        }
    }
    public function get($path, $class, $function)
    {
        if($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $request = $_GET;
            $this->storeRoute($path, $class, $function, $request);
            return $this->routes;
        }
    }

    public function post($path, $class, $function)
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $request = $_POST;
            if(empty($request))
            {
                $request = file_get_contents('php://input');
                $request = json_decode($request);
            }
            $this->storeRoute($path, $class, $function, $request);
        }
    }

    public function storeRoute($path, $class, $function, $request)
    {
        $array = ['path' => $path, 'class' => $class, 'function' => $function, 'request' => $request];
        array_push($this->routes, $array);
    }

    public function getRouteArray()
    {
        return $this->routes;
    }

    public function callClass()
    {
        foreach($this->routes as $index => $route)
        {
            if($route['path'] === $this->base_url)
            {
                $this->foundRoute = true;
                $namespace = '\App\Controllers\\' . $route['class'];
                $class = new $namespace();
                call_user_func(array($class, $route['function']), $route['request']);
                break;
            }
        }

        if($this->foundRoute == false)
        {
            $view = new RenderPage('view/404.php');
            $view->run();
        }
    }
}

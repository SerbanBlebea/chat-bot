<?php
namespace App\Controllers;

use App\RenderPage;

class HomeController
{
    public function index($request)
    {
        $view = new RenderPage('view/home.php');
        $view->setParams($request);
        $view->run();
    }

    public function getSendMessage()
    {
        $view = new RenderPage('view/index.php');
        $view->run();
    }
}

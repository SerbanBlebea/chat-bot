<?php
namespace App\Controllers;

use pimax\Messages\Message;
use pimax\FbBotApp;
use App\Config;
use App\RenderPage;

class SendController
{
    public function sendMessage($request)
    {
        $config = new Config('config/config.php');
        $token = $config->getData('token');

        $message = new Message($request['recipient'], $request['message']);
        $bot = new FbBotApp($token);

        $bot->send($message);

        $view = new RenderPage('view/result.php');
        $view->setParams(['result' => $request['message']]);
        $view->run();
    }
}

<?php
namespace App\Controllers;

use App\RenderPage;
use pimax\Messages\Message;
use pimax\FbBotApp;
use App\Log;
use App\Config;
use App\Models\Message as MessageModel;

class WebhookController
{
    public function validateHook($request)
    {
        $config = new Config('config/config.php');
        $verify_token = $config->getData('verify_token');
        $token = $config->getData('token');

        $hub_verify_token = $request['hub_verify_token'];

        if($verify_token == $hub_verify_token)
        {
            echo $request['hub_challenge'];
        } else {
            $view = new RenderPage('view/result.php');
            $view->setParams(['result' => 'Fail.']);
            $view->run();
        }
    }

    public function index($request)
    {
        $recipient = $request->entry[0]->messaging[0]->sender->id;
        $text = $request->entry[0]->messaging[0]->message->text;
        $optin = $request->entry[0]->messaging[0]->optin->ref;
        $read = $request->entry[0]->messaging[0]->read->watermark;

        $log = new Log('log/messages.txt');
        if(!empty($text))
        {
            $log->logData($text);
            $text = strtolower($text);

            $response = MessageModel::where('question', $text)->first();
            //$log->logData(print_r($response->response,1));

            if(!$response)
            {
                $response->response = 'Sorry! I did not get that';
            }

            $config = new Config('config/config.php');
            $token = $config->getData('token');

            $message = new Message($recipient, $response->response);
            $bot = new FbBotApp($token);
            $bot->send($message);
        }

        if(!empty($optin))
        {
            $log->logData($optin);
        }

        if(!empty($read))
        {
            $date = new DateTime();
            $date->setTimestamp($read);
            $log->logData('time_of_read: ' . $date->format('U = Y-m-d H:i:s'));
        }

    }
}

<?php
namespace App;

class Config
{
    public $file;
    public $config;

    public function __construct($file)
    {
        if (file_exists($file))
        {
            $this->file = $file;
            $this->config = include $this->file;
        }
    }

    public function getData($data)
    {
        return $this->config[$data];
    }

    public function getFullArray()
    {
        return $this->config;
    }
}

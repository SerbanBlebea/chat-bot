<?php
require_once('app.php');

use pimax\Messages\Message;
use pimax\FbBotApp;
use App\Log;

if(!empty($_GET))
{
    $token = $_GET['hub_verify_token'];
    if($verify_token == $token)
    {
        $challenge = $_GET['hub_challenge'];
        echo $challenge;
    } else {
        echo 'tokens do not match.';
    }
}


if(!empty(file_get_contents('php://input')))
{
    //Get and decode post request from Facebok
    $input = file_get_contents('php://input');
    $input = json_decode($input);

    //Get message and sender id from Facebook request
    $recipient = $input->entry[0]->messaging[0]->sender->id;
    $text = $input->entry[0]->messaging[0]->message->text;
    $optin = $input->entry[0]->messaging[0]->optin;
    $read = $input->entry[0]->messaging[0]->read->watermark;

    $log = new Log('log/messages.txt');

    if(!empty($optin))
    {
        $log->logData('optin_id: ' . $recipient);
    }

    if(!empty($read))
    {
        $date = new DateTime();
        $date->setTimestamp($read);
        $log->logData('time_of_read: ' . $date->format('U = Y-m-d H:i:s'));
    }

    if(!empty($text))
    {
        //Write sender's message in the message.txt for refferal
        $file = 'messages.txt';
        $current = file_get_contents($file);
        $current .= $text . '+' . $recipient;
        file_put_contents($file, $current);

        $log->logData($text, 'message.txt');

        //Include expression array
        if (file_exists(__DIR__ . '/../config/texts.php')) {
            $texts = include __DIR__ . '/../config/texts.php';
        }

        $text = strtolower($text);

        //Check if the message matches any expression
        if(array_key_exists($text,$texts))
        {
            $text = $texts[$text];
        } else {
            //Basic default message
            $text = 'Sorry! I did not get that';
        }

        //Create the response message and the Bot
        $message = new Message($recipient, $text);
        $bot = new FbBotApp($token);

        //Send the message
        $bot->send($message);
    }
}

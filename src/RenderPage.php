<?php
namespace App;

class RenderPage
{
    public $file_path;
    public $array;

    public function __construct($file_path)
    {
        $this->file_path = $file_path;
    }

    public function setParams($array)
    {
        $this->array = $array;
    }

    public function run()
    {
        $this->array;
        return require_once($this->file_path);
    }
}

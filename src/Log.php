<?php
namespace App;

class Log
{
    public $file;

    public function __construct($file)
    {
        return $this->file = $file;
    }

    public function logData($data)
    {
        $current = file_get_contents($this->file);
        $current .= '[' . date("l jS \of F Y h:i:s A") . ']:' . $data . ' //// ';
        file_put_contents($this->file, $current);
    }
}

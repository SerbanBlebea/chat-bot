<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Message extends Model
{
    protected $table = 'table_messages';
    protected $fillable = [
        'question',
        'response'
    ];
    public $timestamps = false;
}

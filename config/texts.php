<?php
return [
    'hey' => 'Hello, sir!',
    'how are you?' => 'I am fine, sir',
    'weather in London?' => 'Weather is sunny, sir',
    'are you hungry?' => 'I have just ate something, sir',
    'do you have money?' => 'I have 200£, sir'
];

<html>
<head>
    <title>ChatBot</title>
</head>
<body>
    <div class="fb-send-to-messenger"
        messenger_app_id="107476899940907"
        page_id="1131534556888316"
        data-ref="SERBAN-PASS_THROUGHT"
        color="blue"
        size="xlarge">
    </div>

    <form action='/postMessage' method='POST'>
        <label>Recipient:</label><br />
        <input type='text' name='recipient'/><br />

        <label>Message:</label><br />
        <textarea name='message'></textarea><br />

        <input type='submit' value='Send Message' />
    </form>

    <script>

        window.fbAsyncInit = function() {
            FB.init({
                appId: "107476899940907",
                xfbml: true,
                version: "v2.6"
            });

            FB.Event.subscribe('send_to_messenger', function(e) {
                console.log('hello ' + e.event);
            });
        };



        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>
</body>
</html>

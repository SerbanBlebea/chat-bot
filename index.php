<?php

require_once(dirname(__FILE__) . '/vendor/autoload.php');

use Illuminate\Database\Capsule\Manager as Capsule;
use App\Router;
use App\Config;

$config = new Config('config/config.php');

$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => $config->getData('driver'),
    'host'      => $config->getData('host'),
    'database'  => $config->getData('database'),
    'username'  => $config->getData('username'),
    'password'  => $config->getData('password'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();


$router = new Router();
$router->get('/', 'HomeController', 'index');
$router->post('/webhook', 'WebhookController', 'index');
$router->get('/index', 'HomeController', 'getSendMessage');
$router->get('/webhook', 'WebhookController', 'validateHook');
$router->post('/postMessage', 'SendController', 'sendMessage');
$router->get('/test', 'TestController', 'index');

$router->callClass();
